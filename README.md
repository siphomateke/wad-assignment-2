# WAD621S Assignment 2 - Developer Blog

A developer blog that allows for CRUD operations, user authentication and back-end querying, created for a NUST WAD621S assignment.

> Just want to run the website? **[Jump to the Quick Start](##usage)**

![Screenshot of the home page of developer blog website](./docs/screenshot_home.png)
![Screenshot of a single post from the developer blog website](./docs/screenshot_post.png)
![Screenshot of the backend of the website](./docs/screenshot_backend.png)

## Stack

- Backend: [Directus](https://directus.io/)
- Front-End: [Vue.js 3](https://v3.vuejs.org/)
- Language: TypeScript
- UI Framework: [NaiveUI](https://www.naiveui.com/)
- API: [GraphQL](https://graphql.org/) via [Vue Apollo](https://v4.apollo.vuejs.org/)
- State: [Pinia](https://pinia.esm.dev/)

## Project Structure

- `server/` - [Directus](https://directus.io/) - CMS powering the backend
  - `data.db` - SQLite database used by Directus
  - `.env` - Directus config
- `client/` - [Vue.js 3](https://v3.vuejs.org/) - based frontend
  - `src/`
    - `views/` - Components that render each navigation route using Vue Router
    - `stores/` - [Pinia](https://pinia.esm.dev/) stores to manage Vue's state, particularly authentication
    - `gql/` - [Apollo](https://v4.apollo.vuejs.org/) [GraphQL](https://graphql.org/) client configuration, queries and mutations
    - `components/` - Vue.js components
    - `mixins.ts` - Re-usable functionality for Vue components
    - `utils.ts` - Generic utility functions
    - `router.ts` - [Vue router](https://next.router.vuejs.org/) route configuration

## Usage

### Server

```bash
# Install dependencies
cd server
yarn install
# Start backend-server (Directus)
npx directus start
```

You can now open and view the Directus admin dashboard at <http://localhost:8055/admin>

The admin login credentials are:

- Email: admin@example.com
- Password: Awuhf9wL*R%M5X

Guest login credentials:

- Email: guest@example.com
- Password: @u2pEX%g44cqNR

### Client

```bash
cd client
# Install dependencies
yarn install
# Start client developer server (Vite)
yarn dev
```

The website can now be viewed at <http://localhost:3000/>. The login credentials are the same as those used above for the server.
