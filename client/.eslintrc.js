const path = require('path');

module.exports = {
  root: true,
  env: {
    'vue/setup-compiler-macros': true
  },
  parserOptions: {
    project: path.resolve(__dirname, "./tsconfig.json"),
    tsconfigRootDir: path.resolve(__dirname),
  },
  extends: [
    "@siphomateke/eslint-config-typescript",
    "@vue/typescript/recommended",
    'plugin:vue/vue3-recommended',
  ],
  overrides: [
    {
      files: ["src/**/*.vue"],
      rules: {
        "@typescript-eslint/explicit-module-boundary-types": "off",
      },
    },
  ],
  rules: {
    "@typescript-eslint/lines-between-class-members": "off",
    // We use some custom errors that can't extend Error so this rule must be disabled.
    "@typescript-eslint/no-throw-literal": "off",
    "max-classes-per-file": "off",
    "@typescript-eslint/strict-boolean-expressions": [
      "error",
      {
        allowString: true,
        allowNumber: true,
        allowNullableObject: true,
        allowNullableBoolean: true,
        allowNullableString: true,
        allowNullableNumber: false,
        allowAny: true,
      },
    ],
  },
}