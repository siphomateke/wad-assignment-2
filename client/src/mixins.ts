import { useRouter } from 'vue-router';
import { watch, Ref, computed } from 'vue';
import { useLoadingBar, useMessage } from 'naive-ui';
import type { UseQueryReturn } from '@vue/apollo-composable';
import type { OperationVariables } from '@apollo/client/core';
import { storeToRefs } from 'pinia';
import { Post } from './gql/queries';
import useAuthStore from './stores';

export function useRequireAuth() {
  const message = useMessage();
  const router = useRouter();
  const auth = useAuthStore();
  watch(() => auth.isLoggedIn, (isLoggedIn) => {
    if (!isLoggedIn) {
      router.push('/login');
      message.error('You must be logged in to perform this action');
    }
  }, { immediate: true });
}

export function usePostBelongsToMe<T extends Pick<Post, 'user_created'> | undefined | null>(post: Ref<T>) {
  const auth = storeToRefs(useAuthStore());
  const postBelongsToMe = computed(
    () => auth.userId.value === post.value?.user_created.id,
  );
  return postBelongsToMe;
}

export function useRequireOwnership(postUserId: Ref<string | undefined>) {
  const auth = useAuthStore();
  const router = useRouter();
  const message = useMessage();
  watch(() => ({ loggedInUserId: auth.userId, postUserIdRef: postUserId }), ({ loggedInUserId, postUserIdRef }) => {
    if (postUserIdRef.value) {
      const postBelongsToMe = loggedInUserId === postUserIdRef.value;
      if (!auth.isLoggedIn || !postBelongsToMe) {
        router.push('/login');
        message.error('You are not authorized to perform this action');
      }
    }
  }, { immediate: true });
}

export function useLoadingBarQueryLink<TResult, TVariables extends OperationVariables | undefined>(queryReturn: UseQueryReturn<TResult, TVariables>): UseQueryReturn<TResult, TVariables> {
  const {
    loading, error, onResult, onError,
  } = queryReturn;
  const loadingBar = useLoadingBar();
  watch(() => loading.value, (isLoading) => {
    if (isLoading) {
      loadingBar.start();
    } else if (!error.value) {
      loadingBar.finish();
    } else {
      loadingBar.error();
    }
  }, { immediate: true });
  onResult(() => { loadingBar.finish(); });
  onError(() => { loadingBar.error(); });
  return queryReturn;
}
