import EventEmitter from 'eventemitter3';
import type { LoginResponse } from '../../gql/mutations';

export type AuthEvents = {
  authExpired: void;
  finishLogin: LoginResponse;
};

export const authEvents = new EventEmitter<AuthEvents>();
