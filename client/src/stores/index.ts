import { defineStore } from 'pinia';
import { apolloSystemClient } from '../gql/client';
import { LoggedInUserIdResponse, LOGGED_IN_USER_ID } from '../gql/system/queries';
import { LoginResponse, LOGOUT_USER } from '../gql/mutations';

interface State {
  userId: string | null;
  tokenFetchedAt: number | null;
  accessToken: string | null;
  refreshToken: string | null;
  expires: number | null;
}

const useAuthStore = defineStore('auth', {
  state: (): State => ({
    userId: null,
    tokenFetchedAt: null,
    accessToken: null,
    // TODO: Check refresh token periodically
    refreshToken: null,
    expires: null,
  }),
  getters: {
    isLoggedIn: s => !!s.accessToken,
    tokenIsExpired: ({ tokenFetchedAt, expires }) => {
      if (typeof tokenFetchedAt === 'number' && typeof expires === 'number') {
        return (Date.now() - tokenFetchedAt) > expires;
      }
      return true;
    },
  },
  actions: {
    clearTokens() {
      this.accessToken = null;
      this.tokenFetchedAt = null;
      this.refreshToken = null;
      this.expires = null;

      localStorage.removeItem('access_token');
      localStorage.removeItem('token_fetched_at');
      localStorage.removeItem('refresh_token');
      localStorage.removeItem('access_token_expires');

      localStorage.removeItem('user_id');
      this.userId = null;
    },
    onAuthExpired() {
      this.clearTokens();
      window.location.replace('/login');
    },
    async onFinishLogin(loginData: LoginResponse) {
      this.accessToken = loginData.access_token;
      this.tokenFetchedAt = Date.now();
      this.refreshToken = loginData.refresh_token;
      this.expires = loginData.expires;

      localStorage.setItem('access_token', loginData.access_token);
      localStorage.setItem('token_fetched_at', this.tokenFetchedAt.toString());
      localStorage.setItem('refresh_token', loginData.refresh_token);
      localStorage.setItem('access_token_expires', loginData.expires.toString());

      // Fetch user ID
      const response = await apolloSystemClient.query<LoggedInUserIdResponse>({ query: LOGGED_IN_USER_ID });
      this.userId = response.data.users_me.id;
      localStorage.setItem('user_id', this.userId);
    },
    loadFromStorage() {
      this.accessToken = localStorage.getItem('access_token');
      this.tokenFetchedAt = Number(localStorage.getItem('token_fetched_at'));
      this.refreshToken = localStorage.getItem('refresh_token');
      this.expires = Number(localStorage.getItem('access_token_expires'));
      this.userId = localStorage.getItem('user_id');
    },
    async logout() {
      const response = await apolloSystemClient.mutate({ mutation: LOGOUT_USER, variables: { refresh_token: this.refreshToken } });
      if (response.data.auth_logout) {
        this.clearTokens();
      }
    },
  },
});
export default useAuthStore;
