import { defineStore } from 'pinia';
import type { Post } from '../gql/queries';

interface State {
  // Cached post data
  posts: { [id: string]: Post }
}

const usePostsStore = defineStore('posts', {
  state: (): State => ({
    posts: {},
  }),
  getters: {
    postById() {
      return (id: string) => this.posts[id];
    },
  },
  actions: {
    updatePost(id: string, post: Post) {
      this.posts[id] = post;
    },
  },
});
export default usePostsStore;
