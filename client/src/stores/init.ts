import useAuthStore from '.';
import { authEvents } from './plugins/events';

export function initStoreEvents() {
  const authStore = useAuthStore();
  authEvents.on('authExpired', authStore.onAuthExpired);
  authEvents.on('finishLogin', authStore.onFinishLogin);
}
