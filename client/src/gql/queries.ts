import gql from 'graphql-tag';

export interface Post {
  id: string;
  title: string;
  summary: string;
  user_created: {
    id: string;
    first_name: string;
    last_name: string;
  };
  tags: string[];
  date_created: string;
  status: 'published' | 'draft' | 'archived';
}

export const GET_POSTS_BASIC = gql`
  query getPosts($filter: posts_filter) {
    posts(filter: $filter) {
      id
      title
      summary
      user_created {
        id
        first_name
        last_name
      }
      tags
      date_created
      status
    }
  }
`;

export const SEARCH_POSTS_BASIC = gql`
  query searchPosts($search: String, $filter: posts_filter) {
    posts(search: $search, filter: $filter) {
      id
      title
      summary
      user_created {
        id
        first_name
        last_name
      }
      tags
      date_created
      status
    }
  }
`;

export interface FullPost {
  id: string;
  title: string;
  content: string;
  summary: string;
  user_created: {
    id: string;
    first_name: string;
    last_name: string;
  };
  tags: string[];
  date_created: string;
  status: 'published' | 'draft' | 'archived';
}

export const GET_FULL_POST = gql`
  query getPost($id: ID!) {
    posts_by_id(id: $id) {
      id
      title
      summary
      content
      user_created {
        id
        first_name
        last_name
      }
      tags
      date_created
      status
    }
  }
`;

export const GET_POST_TO_EDIT = gql`
  query getPostToEdit($id: ID!) {
    posts_by_id(id: $id) {
      id
      title
      summary
      content
      tags
      date_created
      user_created {
        id
      }
      status
    }
  }
`;
