import {
  ApolloClient, createHttpLink, InMemoryCache, ApolloLink, fromPromise,
} from '@apollo/client/core';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import { authEvents } from '../stores/plugins/events';
import { LoginResponse, USER_REFRESH } from './mutations';

const baseUrl = import.meta.env.VITE_DIRECTUS_URL?.toString();

const nonAuthorizedOperations = ['loginRefresh', 'login'];

const apolloRefreshTokenClient = new ApolloClient({
  link: ApolloLink.from([createHttpLink({ uri: `${baseUrl}/system` })]),
  cache: new InMemoryCache(),
});

const getNewToken = () => {
  const refreshToken = localStorage.getItem('refresh_token');
  if (typeof refreshToken !== 'string') {
    // If there is no refresh token, throw an error and redirect to login page
    authEvents.emit('authExpired');
    throw new Error('Missing refresh token');
  }
  return apolloRefreshTokenClient.mutate<LoginResponse>({ mutation: USER_REFRESH, variables: { refresh_token: refreshToken } }).then((response) => {
    console.log({ response });
    if (response.data) {
      authEvents.emit('finishLogin', response.data);
    }
    return response.data?.access_token;
  });
};

const errorLink = onError(
  ({
    graphQLErrors, operation, forward,
  }) => {
    console.log(operation.operationName, graphQLErrors);
    if (graphQLErrors && !nonAuthorizedOperations.includes(operation.operationName)) {
      for (const err of graphQLErrors) {
        switch (err.extensions.code) {
          case 'INVALID_CREDENTIALS':
            return fromPromise(
              getNewToken().catch(() => {
                // Assume login is invalid and redirect to login
                authEvents.emit('authExpired');
              }),
            )
              // retry the request, returning the new observable
              .flatMap(() => forward(operation));
          default:
            return undefined;
        }
      }
    }
    return undefined;
  },
);

const authLink = setContext(async (operation, { headers }) => {
  const headersCopy = { ...headers };
  if (!operation.operationName || !nonAuthorizedOperations.includes(operation.operationName)) {
    const token = localStorage.getItem('access_token');
    if (token) {
      headersCopy.authorization = `Bearer ${token}`;
    }
  }
  return {
    headers: headersCopy,
  };
});

const apolloClient = new ApolloClient({
  link: ApolloLink.from([errorLink, authLink, createHttpLink({ uri: baseUrl })]),
  cache: new InMemoryCache(),
});

export default apolloClient;

export const apolloSystemClient = new ApolloClient({
  link: ApolloLink.from([errorLink, authLink, createHttpLink({ uri: `${baseUrl}/system` })]),
  cache: new InMemoryCache(),
});
