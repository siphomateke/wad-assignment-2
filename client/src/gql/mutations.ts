import gql from 'graphql-tag';

export interface LoginResponse {
  access_token: string;
  expires: number;
  refresh_token: string;
}

export const LOGIN_USER = gql`
  mutation login($email: String!, $password: String!) {
    auth_login(email: $email, password: $password) {
      access_token
      refresh_token
      expires
    }
  }`;

export const USER_REFRESH = gql`
  mutation loginRefresh($refresh_token: String!) {
    auth_refresh(refresh_token: $refresh_token) {
      access_token
      refresh_token
    }
  }`;

export const LOGOUT_USER = gql`
  mutation logout($refresh_token: String!) {
    auth_logout(refresh_token: $refresh_token)
  }`;

export const DELETE_POST = gql`
  mutation deletePost($id: ID!) {
    delete_posts_item(id: $id) {
      id
    }
  }
`;

export const UPDATE_POST = gql`
  mutation updatePost(
    $id: ID!
    $title: String!
    $summary: String
    $content: String
    $date_created: Date!
    $status: String!
    $tags: JSON
  ) {
    update_posts_item(
      id: $id
      data: {
        title: $title
        summary: $summary
        content: $content
        date_created: $date_created
        status: $status
        tags: $tags
      }
    ) {
      id
      title
      summary
      content
      date_created
      status
      tags
    }
  }
`;

export const CREATE_POST = gql`
  mutation createPost(
    $title: String!
    $summary: String
    $content: String
    $date_created: Date!
    $status: String!
    $tags: JSON
  ) {
    create_posts_item(
      data: {
        title: $title
        summary: $summary
        content: $content
        date_created: $date_created
        status: $status
        tags: $tags
      }
    ) {
      id
      title
      summary
      content
      date_created
      status
      tags
    }
  }
`;
