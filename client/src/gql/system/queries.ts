import gql from 'graphql-tag';

export interface LoggedInUserIdResponse {
  users_me: {
    id: string;
  };
}

export const LOGGED_IN_USER_ID = gql`
  query getLoggedInUser {
    users_me {
      id
    }
  }
`;

export interface UserProfileResponse {
  id: string;
  first_name: string;
  last_name: string;
  title: string;
  email: string;
}

export const USER_PROFILE = gql`
  query getUserProfile($id: ID!) {
    users_by_id(id: $id) {
      id
      first_name
      last_name
      title
      email
    }
  }
`;
