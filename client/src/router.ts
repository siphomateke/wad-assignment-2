import { createRouter, createWebHistory } from 'vue-router';
import HomePage from './views/Home.vue';
import PostPage from './views/Post.vue';
import LoginPage from './views/Login.vue';
import ProfilePage from './views/Profile.vue';
import EditPostPage from './views/EditPost.vue';
import NewPostPage from './views/CreatePost.vue';
import SearchPosts from './views/SearchPosts.vue';

export default createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage,
    },
    {
      path: '/users/:userId',
      name: 'other-profile',
      component: ProfilePage,
    },
    {
      path: '/profile',
      name: 'profile',
      component: ProfilePage,
    },
    {
      path: '/users/:userId/posts/:postId',
      name: 'post',
      component: PostPage,
    },
    {
      path: '/users/:userId/posts/:postId/edit',
      name: 'edit-post',
      component: EditPostPage,
    },
    {
      path: '/users/:userId/posts/create',
      name: 'create-post',
      component: NewPostPage,
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage,
    },
    {
      path: '/search',
      name: 'search',
      component: SearchPosts,
    },
  ],
});
