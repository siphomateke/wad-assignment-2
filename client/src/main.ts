import { ApolloClients } from '@vue/apollo-composable';
import { createApp, h, provide } from 'vue';
import { createPinia } from 'pinia';
import apolloClient, { apolloSystemClient } from './gql/client';
import App from './App.vue';
import router from './router';
import { initStoreEvents } from './stores/init';

const app = createApp({
  setup() {
    provide(ApolloClients, {
      default: apolloClient,
      system: apolloSystemClient,
    });
  },
  render: () => h(App),
})
  .use(router)
  .use(createPinia());
app.mount('#app');

initStoreEvents();
