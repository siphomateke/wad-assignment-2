export function getPostUrl(userId: string, postId: string) {
  return `/users/${userId}/posts/${postId}`;
}
