module.exports = {
  client: {
    service: {
      name: 'directus',
      url: 'http://localhost:8055/graphql',
    },
    includes: [
      'src/**/*.vue',
      'src/**/*.js',
      'src/**/*.ts',
    ],
  },
}